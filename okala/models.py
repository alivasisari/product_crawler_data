from django.db import models


class OkalaProduct(models.Model):
    name = models.CharField(max_length=250)
    description = models.TextField()
    store_name = models.TextField()
    ok_price = models.IntegerField()
    caption = models.CharField(max_length=250)


class OkalaProductImage(models.Model):
    product = models.ForeignKey(OkalaProduct,
                                on_delete=models.SET_NULL,
                                blank=True,
                                null=True, related_name="product_image_url")
    thumb_image = models.TextField()
