from django.urls import path

from okala.views import *

urlpatterns = [
    path('products/add/', AddProducts.as_view()),
    path('products/', DisplayProducts.as_view()),
]
