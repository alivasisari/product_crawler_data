from rest_framework.renderers import TemplateHTMLRenderer
from rest_framework.response import Response
from rest_framework.views import APIView
from django.http import HttpResponse
from okala.models import OkalaProduct, OkalaProductImage


class AddProducts(APIView):
    def post(self, request):
        # Get data and save to database table
        print(request.data)
        for product in request.data["Products"]:
            okala_product = OkalaProduct(name=product["name"], description=product["description"],
                                         store_name=product["storeName"]
                                         , ok_price=product["okPrice"],
                                         caption=product["caption"])
            image = OkalaProductImage(product=okala_product,
                                      thumb_image=product["thumbImage"])
            okala_product.save()
            image.save()

        return HttpResponse(status=200)


class DisplayProducts(APIView):
    renderer_classes = [TemplateHTMLRenderer]
    template_name = 'okala/products.html'

    def get(self, request):
        # Get data from database tables
        # display data in template
        products = OkalaProduct.objects.all().values("product_image_url__thumb_image", "name",
                                                     "description", "id", "caption", "store_name", "ok_price")
        context = {'products': products}
        return Response(context)
