# Generated by Django 4.0.6 on 2022-07-19 19:45

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('tezolmarket', '0002_alter_tezolmarketproductimage_product'),
    ]

    operations = [
        migrations.AlterField(
            model_name='tezolmarketproduct',
            name='is_available_text',
            field=models.TextField(),
        ),
    ]
