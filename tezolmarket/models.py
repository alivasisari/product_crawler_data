from django.db import models


class TezolmarketProduct(models.Model):
    name = models.CharField(max_length=250)
    full_name = models.CharField(max_length=250)
    subtitle = models.TextField()
    is_available_text = models.TextField()
    final_unit_price = models.FloatField()


class TezolmarketProductImage(models.Model):
    product = models.ForeignKey(TezolmarketProduct,
                                on_delete=models.SET_NULL,
                                blank=True,
                                null=True, related_name="product_image_url")
    large_image_url = models.TextField()
