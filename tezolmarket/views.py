from django.shortcuts import render
from django.http import JsonResponse
from rest_framework.renderers import TemplateHTMLRenderer
from rest_framework.response import Response
from rest_framework.views import APIView
from django.http import HttpResponse
from tezolmarket.models import TezolmarketProduct, TezolmarketProductImage


class AddProducts(APIView):
    def post(self, request):
        # Get data and save to database table
        for product in request.data["Products"]:
            tezolmarket_product = TezolmarketProduct(name=product["Name"], subtitle=product["Subtitle"],
                                                     full_name=product["FullName"]
                                                     , is_available_text=product["IsAvailableText"],
                                                     final_unit_price=product["FinalUnitPrice"])
            image = TezolmarketProductImage(product=tezolmarket_product,
                                                large_image_url=product["LargImageUrl"])
            tezolmarket_product.save()
            image.save()

        return HttpResponse(status=200)


class DisplayProducts(APIView):
    renderer_classes = [TemplateHTMLRenderer]
    template_name = 'tezolmarket/products.html'

    def get(self, request):
        # Get data from database tables
        # display data in template
        products = TezolmarketProduct.objects.all().values("product_image_url__large_image_url","final_unit_price", "full_name", "id", "is_available_text", "name", "product_image_url", "subtitle")
        context = {'products': products}
        return Response(context)
